<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use Session;
use App\User;
use Alert;

class AuthController extends Controller
{
    public function login(){
        return view('auth-login');
    }

    public function authenticate(Request $request){

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            Alert::success('Welcome To Sistem', 'Success');
            return redirect()->intended('/');
        }

        Alert::error('Your Login Failed', 'Error');
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
            'password' => 'The provided credentials do not match our records.',
        ]);

    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        Alert::info('Your Logout has been successfully', 'Success');
        return Redirect('/login');
    }

    public function forgot(){
        return view ('admin.forgot-password');
    }

    public function forgotPass(Request $request){
        $dataUser = User::where('email', $request->email)->first();
        if (!empty($dataUser)){
            $dataUser->password = bcrypt($request->password);
            $dataUser->save();
            Alert::success('Your Password Updated has been successfully', 'Success');
            return redirect('/login');
        }

        Alert::success('Your Password Updated has been successfully', 'Success');
        return redirect()->back();
    }
}
