<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterProduk;
use App\Models\MasterSatuan;
use App\Models\MasterUsia;
use App\Models\MasterKategori;
use Carbon\Carbon;
use App\Exports\ProdukTemplate;
use App\Exports\ProdukExport;
use App\Imports\ProdukImport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use DB;

class MasterDataController extends Controller
{
    public function index(){
        $dataProduk = MasterProduk::with('kategori')->get();
        $produk = $dataProduk->groupBy('kode')->map(function ($row) {
            return [
                'id' => $row->first()->id,
                'duplicate' => $row->count('kode'),
                'kode' => $row->first()->kode,
                'kategori' => $row->first()->kategori['nama'],
                'nama' => $row->first()->nama,
                'usia' => $row->first()->usia,
                'qty_mg' => $row->first()->qty_mg,
                'satuan' => $row->first()->satuan,
                'harga_beli' => $row->first()->harga_beli,
                'harga_jual' => $row->first()->harga_jual,
            ];
        })->values();

        $satuan = MasterSatuan::get();
        $usia = MasterUsia::get();
        $kategori = MasterKategori::get();
        return view('admin.produk', compact('produk','satuan','usia','kategori'));
    }

    public function store(Request $request){
        $nama = $request->nama;
        $explod = explode(' ', $nama);
        $text = [];
        if (count($explod) > 1) {
            foreach ($explod as $key => $value) {
                $text[] = substr($value, 0, 1);
            }
            $split = implode($text);
        }else{
            $split = substr($explod[0], 0, 3);
        }
        $noKode = strtoupper($split.''.$request->qty_mg.''.$request->satuan);
        $produk = new MasterProduk;
        $produk->kode = $noKode;
        $produk->kategori_id = $request->kategori_id;
        $produk->nama = $nama;
        $produk->usia = $request->usia;
        $produk->qty_mg = $request->qty_mg;
        $produk->satuan = $request->satuan;
        $produk->harga_beli = $request->harga_beli;
        $produk->harga_jual = $request->harga_jual;
        $produk->created_by = auth()->user()->name;
        $produk->save();
        Alert::success('Congrats', 'Created Has been successfully');
        return redirect('/master-produk');
    }

    public function show($id){
        $produk = MasterProduk::with('kategori')->where('id', $id)->first();
        $satuan = MasterSatuan::get();
        $usia = MasterUsia::get();
        $kategori = MasterKategori::get();
        return view('admin.produk-show', compact('produk','satuan', 'usia', 'kategori'));

    }

    public function update(Request $request, $id){
        $produk = MasterProduk::where('id', $id)->first();
        $produk->kode = $request->kode;
        $produk->nama = $request->nama;
        $produk->usia = $request->usia;
        $produk->qty_mg = $request->qty_mg;
        $produk->satuan = $request->satuan;
        $produk->harga_beli = $request->harga_beli;
        $produk->harga_jual = $request->harga_jual;
        $produk->save();
        Alert::success('Congrats', 'Updated Has been successfully');
        return redirect('/master-produk');

    }

    public function destroy(Request $request){
        $produk = MasterProduk::find($request->id);
        $produk->delete();
        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect('/master-produk');
    }

    public function template(Request $request){
        return Excel::download(new ProdukTemplate($request), 'master-produk-template.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function export(Request $request){
        return Excel::download(new ProdukExport($request), 'master-produk.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function import(Request $request)
    {
        $file = $request->file('file');
        config(['excel.import.startRow' => 3]);
        Excel::import(new ProdukImport, $file, \Maatwebsite\Excel\Excel::XLSX);

        Alert::success('Congrats', 'Imported Has been successfully');
        return redirect('/master-produk');
    }
}
