<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\TempStock;
use App\Models\MasterProduk;
use Alert;

class StockController extends Controller
{
    public function index(){
        $stock = Stock::get();
        return view('admin.stock', compact('stock'));
    }

    public function tempStock(){
        $temp_stock = TempStock::get();
        $produk = MasterProduk::get();

        Alert::success('Congrats', 'Created Has been successfully');
        return view('admin.stock-add', compact('temp_stock', 'produk'));
    }

    public function tempStore(Request $request, $id){
        $produk  = MasterProduk::where('id', $id)->first();

        $tempStock = new TempStock;
        $tempStock->kode_produk = $produk->kode;
        $tempStock->nama_produk = $produk->nama;
        $tempStock->stock = 0;
        $tempStock->save();
        Alert::success('Congrats', 'Created Has been successfully');
        return redirect()->route('stock.temp');
    }

    public function tempHapus($id){
        $tempStock = TempStock::find($id);
        $tempStock->delete();
        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect()->route('stock.temp');
    }

    //stock store
    public function store(Request $request){
        foreach ($request['kode_produk'] as $key => $value) {
            // cek stock sebelumnya
            $befStock = Stock::where('kode_produk', $request['kode_produk'][$key])->first();
            $stock = $request['stock'][$key];
            if (!empty($befStock)) {
                $befStock->stock_awal = $befStock->stock_awal + $stock;
                $befStock->stock_masuk = $befStock->stock_masuk + $stock;
                $befStock->stock_sisa = $befStock->stock_sisa + $stock;
                $befStock->save();
            }else{
                $stocks = new Stock;
                $stocks->kode_produk = $request['kode_produk'][$key];
                $stocks->nama_produk = $request['nama_produk'][$key];
                $stocks->stock_awal = $stock;
                $stocks->stock_masuk = $stock;
                $stocks->stock_keluar = $stock;
                $stocks->stock_sisa = $stock;
                $stocks->save();
            }
        }

        // deleted temp_stock
        $tempPesanan = TempStock::orderBy('id', 'desc')->get();
        foreach ($tempPesanan as $key => $value) {
            $temp = TempStock::where('id', $value['id'])->first();
            $temp->delete();
        }

        Alert::success('Congrats', 'Created Has been successfully');
        return redirect()->route('stock.index');
    }

    public function show($id){
        $stock = Stock::where('id', $id)->first();
        return view('admin.stock-show', compact('stock'));
    }

    public function update(Request $request, $id){
        $stocks = Stock::where('id', $id)->first();
        $stocks->nama_produk = $request->nama_produk;
        $stocks->stock_awal = $request->stock_awal;
        $stocks->stock_masuk = $request->stock_masuk;
        $stocks->stock_keluar = $request->stock_keluar;
        $stocks->stock_sisa = $request->stock_sisa;
        $stocks->save();
        Alert::success('Congrats', 'Updated Has been successfully');
        return redirect()->route('stock.index');
    }

}
