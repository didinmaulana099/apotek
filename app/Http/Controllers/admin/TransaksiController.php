<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterProduk;
use App\Models\MasterKategori;
use App\Models\TempPesanan;
use App\Models\Pesanan;
use App\Models\Transaksi;
use App\Models\Stock;
use Carbon\Carbon;
use Alert;
use App\Exports\TransaksiExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use Mike42\Escpos;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintBuffers\ImagePrintBuffer;
use Mike42\Escpos\CapabilityProfiles\DefaultCapabilityProfile;
use Mike42\Escpos\CapabilityProfiles\SimpleCapabilityProfile;

class TransaksiController extends Controller
{
    public function scan(Request $request){
        $produk  = MasterProduk::with('kategori','stock')
        ->orderBy('id', 'desc')
        ->get();
        $temp_pesanan = TempPesanan::get();
        return view('admin.scan', compact('produk', 'temp_pesanan'));
    }
    
    public function tempPesanan($id){
        $produk  = MasterProduk::with('kategori')
        ->where('id', $id)
        ->first();
        
        $tempPesanan = new TempPesanan;
        $tempPesanan->kode_produk = $produk->kode;
        $tempPesanan->produk_id = $produk->id;
        $tempPesanan->kategori_id = $produk->kategori['id'];
        $tempPesanan->nama_produk = $produk->nama;
        $tempPesanan->satuan = $produk->qty_mg.' '.$produk->satuan;
        $tempPesanan->harga = $produk->harga_jual;
        $tempPesanan->qty = 0;
        $tempPesanan->save();
        Alert::success('Congrats', 'Created Has been successfully');
        return redirect()->route('transaksi.scan');
    }
    
    public function hapusPesanan($id){
        $tempPesanan = TempPesanan::where('id', $id)->first();
        $tempPesanan->delete();
        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect()->route('transaksi.scan');
    }
    
    public function trxPesanan(Request $request){
        $trxLast = Transaksi::orderBy('id', 'desc')->first();
        if (!empty($trxLast)) {
            $number = intval($trxLast->number) + 1;
            $inc = str_pad($number, 3, '0', STR_PAD_LEFT);
        }else{
            $inc = '001';
        }
        $noTrx = 'INV'.Carbon::now()->format('y').''.$inc;
        $trx = new Transaksi;
        $trx->no_trx = $noTrx;
        $trx->jumlah_item = count($request['kode_produk']);
        $trx->grand_total = $request->subtotal;
        $trx->cash = $request->cash;
        $trx->change = $request->change;
        $trx->number = $inc;
        $trx->created_by = auth()->user()->name;
        $trx->save();
        foreach ($request['kode_produk'] as $key => $value) {
            $pesanan = new Pesanan;
            $pesanan->transaksi_pesanan_id = $trx->id;
            $pesanan->kode_produk = $request['kode_produk'][$key];
            $pesanan->kategori_id = $request['kategori_id'][$key];
            $pesanan->produk_id = $request['produk_id'][$key];
            $pesanan->nama_produk = $request['nama_produk'][$key];
            $pesanan->satuan = $request['satuan'][$key];
            $pesanan->harga = $request['harga'][$key];
            $pesanan->qty = $request['qty'][$key];
            $pesanan->jumlah = $request['jumlah'][$key];
            $pesanan->created_by = auth()->user()->name;
            $pesanan->save();
            
            // update stock
            $stock = Stock::where('kode_produk', $request['kode_produk'][$key])->first();
            $stock->stock_masuk = $stock->stock_masuk - $request['qty'][$key];
            $stock->stock_keluar = $stock->stock_keluar + $request['qty'][$key];
            $stock->stock_sisa = $stock->stock_sisa - $request['qty'][$key];
            $stock->save();
        }
        
        // deleted temp_pesanan
        $tempPesanan = TempPesanan::orderBy('id', 'desc')->get();
        foreach ($tempPesanan as $key => $value) {
            $temp = TempPesanan::where('id', $value['id'])->first();
            $temp->delete();
        }
        
        $transaksi = $trx;
        return $this->printProses($transaksi);
        // // cetak pesanan
        // // $connector = new FilePrintConnector("php://stdout");
        // // $printer = new Printer($connector);
        // // $printer->text("Hello World!\n");
        // // $printer->cut();
        // // $printer->close();
        // $pdf = PDF::loadview('admin.transaksi-cetak',['transaksi' => $trx])->setPaper('A8', 'portrait');
        // return $pdf->download($trx->no_trx.'.pdf');
        // // Alert::success('Congrats', 'Created Has been successfully');
        // return redirect()->route('transaksi.cetak', $trx->id);
    }
    
    public function list(Request $request){
        $trx = Transaksi::with('pesanan')->orderBy('created_at', 'desc')->get();
        return view('admin.transaksi-list', compact('trx'));
    }
    
    public function show($id){
        $trx = Transaksi::find($id);
        $produk  = MasterProduk::with('kategori','stock')
        ->orderBy('id', 'desc')
        ->get();
        return view('admin.transaksi-edit', compact('trx','produk'));
    }
    
    
    // hapus pesanan ditampilan edit
    public function editHapusPesananTrx($id){
        $pesanan = Pesanan::where('id', $id)->first();
        $trx = Transaksi::where('id', $pesanan->transaksi_pesanan_id)->first();
        
        // updated stock
        $stock = Stock::where('kode_produk', $pesanan->kode_produk)->first();
        $stock->stock_masuk = $stock->stock_masuk + $pesanan->qty;
        $stock->stock_keluar = $stock->stock_keluar + $pesanan->qty;
        $stock->stock_sisa = $stock->stock_sisa + $pesanan->qty;
        $stock->save();
        
        $pesanan->delete();
        
        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect()->route('transaksi.show', $trx->id);
    }
    
    // tambah pesanan ditampilan edit transaksi
    public function editAddPesanan(Request $request, $id){
        $produk = MasterProduk::where('id', $id)->first();
        $trx = Transaksi::where('id', $request->id_trx)->first();
        
        $pesanan = Pesanan::firstOrNew(
            [
                'transaksi_pesanan_id' =>  $request->id_trx,
                'kode_produk' => $produk->kode
            ],
        );
        $pesanan->transaksi_pesanan_id = $request->id_trx;
        $pesanan->kode_produk = $produk->kode;
        $pesanan->produk_id = $produk->id;
        $pesanan->kategori_id = $produk->kategori_id;
        $pesanan->nama_produk = $produk->nama;
        $pesanan->satuan = $produk->qty_mg.' '.$produk->satuan;
        $pesanan->harga = $produk->harga_jual;
        $pesanan->qty = 0;
        $pesanan->save();
        Alert::success('Congrats', 'created Has been successfully');
        return redirect()->route('transaksi.show', $trx->id);
    }
    
    // update pesanan ditampilan transaksi
    public function update(Request $request, $id){
        //updated ke table tranksaksi
        $trx = Transaksi::where('id', $id)->first();
        $trx->jumlah_item = count($request['kode_produk']);
        $trx->grand_total = $request->subtotal;
        $trx->cash = $request->cash;
        $trx->change = $request->change;
        $trx->created_by = auth()->user()->name;
        $trx->save();
        foreach ($request['kode_produk'] as $key => $value) {
            $pesanan = Pesanan::firstOrNew(
                [
                    'transaksi_pesanan_id' =>  $id,
                    'kode_produk' => $request['kode_produk'][$key]
                ],
            );
            $pesanan->transaksi_pesanan_id = $id;
            $pesanan->kode_produk = $request['kode_produk'][$key];
            $pesanan->kategori_id = $request['kategori_id'][$key];
            $pesanan->produk_id = $request['produk_id'][$key];
            $pesanan->nama_produk = $request['nama_produk'][$key];
            $pesanan->satuan = $request['satuan'][$key];
            $pesanan->harga = $request['harga'][$key];
            $pesanan->qty = $request['qty'][$key];
            $pesanan->jumlah = $request['jumlah'][$key];
            $pesanan->created_by = auth()->user()->name;
            $pesanan->save();
            
            // update stock
            $stock = Stock::where('kode_produk', $request['kode_produk'][$key])->first();
            $stock->stock_masuk = $stock->stock_masuk - $request['qty'][$key];
            $stock->stock_keluar = $stock->stock_keluar + $request['qty'][$key];
            $stock->stock_sisa = $stock->stock_sisa - $request['qty'][$key];
            $stock->save();
        }
        
        Alert::success('Congrats', 'updated has been successfully');
        return redirect()->route('transaksi.list');
    }
    
    public function hapusTrx($id){
        $trx = Transaksi::find($id);
        $pesanan = Pesanan::where('transaksi_pesanan_id', $trx->id)->get();
        foreach ($pesanan as $key => $value) {
            $psn = Pesanan::where('id', $value['id'])->first();
            $psn->delete();
        }
        $trx->delete();
        
        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect()->route('transaksi.list');
    }
    
    public function details($id){
        $trx = Transaksi::with('pesanan')->where('id', $id)->first();
        return view('admin.transaksi-details', compact('trx'));
    }

    public function trxCetak(Request $request, $id){
        $transaksi = Transaksi::with('pesanan')->where('id', $id)->first();
        return $this->printProses($transaksi);
    }
    
    public function export(Request $request){
        return Excel::download(new TransaksiExport($request), 'transaksi'.Carbon::parse($request->start_date)->format('dmy').'.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
    
    public function report(Request $request){
        $index = Transaksi::with('pesanan')->where(function ($where) use ($request) {
            if (!empty($request->start_date) && !empty($request->end_date)) {
                $where->whereBetween('created_at', [
                    Carbon::parse(date($request->start_date). '00:00:00'),
                    Carbon::parse(date($request->end_date). '23:59:59')
                ]);
            }
        });
        
        $unmap = (clone $index)
        ->get();
        
        $transaksi = $unmap->map(function ($item, $key) {
            return $item->pesanan->map(function ($psn) use ($item){
                return [
                    // 'item' => $item,
                    'tgl_trx' => $item->created_at,
                    'no_trx' => $item->no_trx,
                    'grand_total' => $item->grand_total,
                    'kode_produk' => $psn->kode_produk,
                    'nama_produk' => $psn->nama_produk,
                    'satuan' => $psn->satuan,
                    'harga' => $psn->harga,
                    'qty' => $psn->qty,
                    'jumlah' => $psn->jumlah,
                    'created_by' => $psn->created_by,
                ];
            });
        })
        ->collapse();
        
        $report = json_decode($transaksi);
        return view('admin.report', compact('report'));
    }


    function addSpaces($string = '', $valid_string_length = 0) {
        if (strlen($string) < $valid_string_length) {
            $spaces = $valid_string_length - strlen($string);
            for ($index1 = 1; $index1 <= $spaces; $index1++) {
                $string = $string . ' ';
            }
        }
        
        return $string;
    }
    
    // proses print
    function printProses($transaksi){

        $connector = new WindowsPrintConnector("POS-58");
        $printer = new Printer($connector);
        
        $printer->initialize();

        // $printer->setFont(Printer::FONT_B);
        // $printer->setJustification(Printer::JUSTIFY_CENTER);
        // $printer->text(Carbon::now()->format('d/m/Y H:i:s'). "\n");
        // $printer->setLineSpacing(5);
        // $printer->text("\n");

        // $printer->initialize();
        $printer->setFont(Printer::FONT_A);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("Apotek Lentera Farma \n");

        // $printer->initialize();
        $printer->setFont(Printer::FONT_B);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("Jl. Tanggul Irigasi Bendasari satu \n RT008/004 Kel. Kondang Jaya \n Kec. Karawang Timur. Jawa Barat \n" . "\n");
        $printer->setLineSpacing(5);
        $printer->text("\n");

        // $printer->initialize();
        $printer->setFont(Printer::FONT_B);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        // $printer->text($transaksi->no_trx);
        $printer->text($this->addSpaces($transaksi->no_trx, 14) . Carbon::now()->format('d/m/Y H:i:s'));
        
        $printer->feed();
        $printer->setPrintLeftMargin(0);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setEmphasis(true);
        // $printer->text($this->addSpaces($transaksi->no_trx, 10) . Carbon::now()->format('d/m/Y H:i:s'));
        // $printer->text($this->addSpaces('Item', 19) . $this->addSpaces('Qty', 5) . $this->addSpaces('Jumlah', 8));
        $printer->text("--------------------------------" . "\n");

        $printer->setEmphasis(false);
        $items = [];
        foreach ($transaksi->pesanan as $value) {
            $items[] = [
                'name' => $value['nama_produk'],
                'qty' => $value['qty'],
                'jumlah' => number_format($value['jumlah']),
            ];
        }

        foreach ($items as $item) {
            //Current item ROW 1
            $name_lines = str_split($item['name'], 15);
            foreach ($name_lines as $k => $l) {
                $l = trim($l);
                $name_lines[$k] = $this->addSpaces($l, 19);
            }
            
            $qty = str_split($item['qty'], 10);
            foreach ($qty as $k => $l) {
                $l = trim($l);
                $qty[$k] = $this->addSpaces($l, 5);
            }
            
            $jumlah = str_split($item['jumlah'], 12);
            foreach ($jumlah as $k => $l) {
                $l = trim($l);
                $jumlah[$k] = $this->addSpaces($l, 8);
            }
            
            $counter = 0;
            $temp = [];
            $temp[] = count($name_lines);
            $temp[] = count($qty);
            $temp[] = count($jumlah);
            $counter = max($temp);
            
            for ($i = 0; $i < $counter; $i++) {
                $line = '';
                if (isset($name_lines[$i])) {
                    $line .= ($name_lines[$i]);
                }
                if (isset($qty[$i])) {
                    $line .= ($qty[$i]);
                }
                if (isset($jumlah[$i])) {
                    $line .= ($jumlah[$i]);
                }
                // $printer->initialize();
                $printer->setJustification(Printer::JUSTIFY_CENTER);
                $printer->setTextSize(8, 6);
                $printer->text($line);
            }
            
            $printer->feed();
        }
        $printer->text("--------------------------------" . "\n");
        
        // $printer->initialize();
        $printer->setPrintLeftMargin(10);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setTextSize(8, 6);
        $printer->text($this->addSpaces("", 11) ."Total   : "."Rp.".number_format($transaksi->grand_total) . "\n");
        $printer->text($this->addSpaces("", 11) ."Cash    : "."Rp.".number_format($transaksi->cash) . "\n");
        $printer->text($this->addSpaces("", 11) ."Kembali : "."Rp.".number_format($transaksi->change) . "\n");
        $printer->text("\n\n");
        
        // $printer->initialize();
        $printer->setFont(Printer::FONT_A);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("\n");
        $printer->text("\n Terima Kasih \n");
        $printer->text("\n");
        
        $printer->setFont(Printer::FONT_B);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("--------------------------------" . "\n");
        $printer->text("Develop By ETTOS \n");
        $printer->text("\n");
        
        $printer->cut();
        $printer->pulse();
        $printer->close();

        Alert::success('Congrats', 'Printed Has been successfully');
        return redirect()->route('transaksi.list');
    }

    
}
