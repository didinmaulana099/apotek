<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;
use Carbon\Carbon;
use DB;


class DashboardController extends Controller
{
    public function dashboard(Request $request){
        $trx = DB::table('transaksi_pesanan');
        $monthly = $trx->whereMonth('created_at', Carbon::now())->get();
        $annualy = $trx->whereYear('created_at', Carbon::now())->get();
        $weekly = Transaksi::where( 'created_at', '>', Carbon::now()->subDays(7))->get();
        $daily = $trx->whereDay('created_at', Carbon::now())->get();
        $recent = Transaksi::orderBy('created_at', 'desc')->get();

        $tahun = Carbon::now()->format('Y'); //Mengambil tahun saat ini
        $bulan = Carbon::now()->format('m'); //Mengambil bulan saat ini
        $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        $tgl = [];
        $jumlah = [];
        for ($i=1; $i < $tanggal+1; $i++) {
            $tgl[] = $i;
            $daySum = DB::table('transaksi_pesanan')->whereDay('created_at', $i)->sum('grand_total');
            $jumlah[] = (int)$daySum;
        }

        $report = [
            'tanggal' => $tgl,
            'jumlah_hari' => $jumlah,
            'recent_trx' => $recent->take(3),
            'dailyCount' => $daily->count(),
            'dailySum' => number_format($daily->sum('grand_total')),
            'weeklyCount' => $weekly->count(),
            'weeklySum' => number_format($weekly->sum('grand_total')),
            'monthlyCount' => $monthly->count(),
            'monthlySum' => number_format($monthly->sum('grand_total')),
            'annualyCount' => $annualy->count(),
            'annualySum' => number_format($annualy->sum('grand_total')),
        ];

        return view('admin.dashboard', compact('report'));
    }
}
