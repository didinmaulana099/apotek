<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterKategori;
use Carbon\Carbon;
use App\Exports\KategoriExport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;

class MasterKategoriController extends Controller
{
    public function index(){
        $kategori = MasterKategori::get();
        return view('admin.kategori', compact('kategori'));
    }

    public function store(Request $request){
        $kategori = new MasterKategori;
        $kategori->nama = $request->nama;
        $kategori->keterangan = $request->keterangan;
        $kategori->save();
        Alert::success('Congrats', 'Created Has been successfully');
        return redirect('/master-kategori');
    }

    public function show($id){
        $kategori = MasterKategori::where('id', $id)->first();
        return view('admin.kategori-show', compact('kategori'));

    }

    public function update(Request $request, $id){
        $kategori = MasterKategori::where('id', $id)->first();
        $kategori->nama = $request->nama;
        $kategori->keterangan = $request->keterangan;
        $kategori->save();
        Alert::success('Congrats', 'Updated Has been successfully');
        return redirect('/master-kategori');

    }

    public function destroy(Request $request){
        $kategori = MasterKategori::find($request->id);
        $kategori->delete();

        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect('/master-kategori');
    }

    public function export(Request $request){
        Alert::success('Congrats', 'Exported Has been successfully');
        return Excel::download(new KategoriExport($request), 'master-kategori.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

}
