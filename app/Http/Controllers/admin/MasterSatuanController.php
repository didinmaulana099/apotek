<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterSatuan;
use Alert;

class MasterSatuanController extends Controller
{
    public function index(){
        $satuan = MasterSatuan::get();
        return view('admin.satuan', compact('satuan'));
    }

    public function store(Request $request){
        $satuan = new MasterSatuan;
        $satuan->nama = $request->nama;
        $satuan->save();

        Alert::success('Congrats', 'Created Has been successfully');
        return redirect()->route('satuan.index');
    }

    public function show($id){
        $satuan = MasterSatuan::find($id);
        return view('admin.satuan-show', compact('satuan'));
    }

    public function update(Request $request, $id){
        $satuan = MasterSatuan::find($id);
        $satuan->nama = $request->nama;
        $satuan->save();
        Alert::success('Congrats', 'Updated Has been successfully');
        return redirect()->route('satuan.index');
    }

    public function destroy($id){
        $satuan = MasterSatuan::find($id);
        $satuan->delete();
        Alert::success('Congrats', 'Deleted Has been successfully');
        return redirect()->route('satuan.index');
    }
}
