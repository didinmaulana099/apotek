<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterUsia;
use Alert;

class MasterUsiaController extends Controller
{
    public function index(){
        $usia = MasterUsia::get();
        return view('admin.usia', compact('usia'));
    }

    public function store(Request $request){
        $usia = new Masterusia;
        $usia->nama = $request->nama;
        $usia->save();
        Alert::success('Congrats', 'Created Has been successfully');
        return redirect()->route('usia.index');
    }

    public function show($id){
        $usia = Masterusia::find($id);
        return view('admin.usia-show', compact('usia'));
    }

    public function update(Request $request, $id){
        $usia = Masterusia::find($id);
        $usia->nama = $request->nama;
        $usia->save();
        Alert::success('Congrats', 'Updated Has been successfully');
        return redirect()->route('usia.index');
    }

    public function destroy($id){
        $usia = Masterusia::find($id);
        $usia->delete();
        Alert::success('Congrats', 'Deleteed Has been successfully');
        return redirect()->route('usia.index');
    }
}
