<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next)
    // {
    //     return $next($request);
    // }

    public function handle(Request $request, Closure $next,...$role)
    {
        // if (\Auth::user() &&  \Auth::user()->role == $role) {
        //     return $next($request);
        // }
        if (in_array($request->user()->role, $role)) {
            return $next($request);
        }
        return redirect ('/login');
    }
}
