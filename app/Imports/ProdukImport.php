<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Models\MasterProduk;
use App\Models\MasterKategori;
use App\Models\MasterUsia;
use App\Models\MasterSatuan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ProdukImport implements ToCollection, WithHeadingRow, WithStartRow
{
    /**
    * @return int
    */
    public function startRow(): int
    {
        return 3;
    }

    /**
    * @param array $row
    * @param Collection $collection
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {

        // Validator::make($rows->toArray(), [
            //     '*.username' => 'required',
            //     '*.email' => 'required',
            //     '*.password' => 'required',
            // ])->validate();

            foreach ($rows as $row) {
                //cek kategori
                $kategoris = MasterKategori::where('nama', $row['kategori'])->first();
                if (empty($kategoris)) {
                    $kategoriNew = MasterKategori::firstOrCreate(
                        ['nama' => $row['kategori']],
                    );
                    $kategoriNew->nama = $row['kategori'];
                    $kategoriNew->keterangan = $row['keterangan'];
                    $kategoriNew->save();
                }
                // cek usia
                if (!empty($row['usia'])) {
                    $reqUsia = $row['usia'];
                }else{
                    $reqUsia = 'Umum';
                }
                $usias = MasterUsia::where('nama', $reqUsia)->first();
                $usiaNew = MasterUsia::firstOrCreate(
                    ['nama' => $reqUsia],
                );
                $usiaNew->nama = $reqUsia;
                $usiaNew->save();

                // cek satuan
                if (!empty($row['satuan'])) {
                    $reqSatuan = $row['satuan'];
                }else{
                    $reqSatuan = 'Nl';
                }
                $satuans = Mastersatuan::where('nama', $reqSatuan)->first();
                $satuanNew = Mastersatuan::firstOrCreate(
                    ['nama' => $reqSatuan],
                );
                $satuanNew->nama = $reqSatuan;
                $satuanNew->save();

                $nama = $row['nama'];
                $explod = explode(' ', $nama);
                $text = [];
                if (count($explod) > 1) {
                    foreach ($explod as $key => $value) {
                        $text[] = substr($value, 0, 1);
                    }
                    $split = implode($text);
                }else{
                    $split = substr($explod[0], 0, 3);
                }

                $qtyMg = !empty($row['qty_mg']) ? $row['qty_mg'] : 00;
                $noKode = strtoupper($split.''.$qtyMg.''.$reqSatuan);

                $item = MasterProduk::firstOrCreate(
                    ['kode' => $noKode],
                );
                $item->kode = $noKode;
                $item->nama = $row['nama'];
                $item->qty_mg = $qtyMg;
                $item->satuan = !empty($satuans) ? $satuans->nama : $satuanNew->nama;
                $item->usia = !empty($usias) ? $usias->nama : $usiaNew->nama;
                $item->harga_jual = !empty($row['harga_jual']) ? $row['harga_jual'] : 0;
                $item->harga_beli = !empty($row['harga_beli']) ? $row['harga_beli'] : 0;
                $item->kategori_id = !empty($kategoris) ? $kategoris->id : $kategoriNew->id;
                $item->created_by = !empty(auth()->user()->name) ? auth()->user()->name : 'admin';
                $item->save();
            }

        }
    }
