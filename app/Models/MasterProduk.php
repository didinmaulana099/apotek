<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterProduk extends Model
{
    //
    protected $table = 'master-produk';

    public function kategori()
    {
        return $this->belongsTo('App\Models\MasterKategori', 'kategori_id', 'id');
    }

    public function stock()
    {
        return $this->belongsTo('App\Models\Stock', 'kode', 'kode_produk');
    }
}
