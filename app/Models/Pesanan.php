<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    //
    protected $table = 'pesanan';
    protected $fillable = ['transaksi_pesanan_id', 'kode_produk'];
}
