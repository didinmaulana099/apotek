<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterUsia extends Model
{
    //
    protected $table = 'master-usia';
    protected $fillable = ['nama'];
}
