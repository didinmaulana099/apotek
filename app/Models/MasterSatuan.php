<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterSatuan extends Model
{
    protected $table = 'master-satuan';
    protected $fillable = ['nama'];
}
