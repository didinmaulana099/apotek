<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    //
    protected $table = 'transaksi_pesanan';

    public function pesanan()
    {
        return $this->hasMany('App\Models\Pesanan', 'transaksi_pesanan_id', 'id');
    }
}
