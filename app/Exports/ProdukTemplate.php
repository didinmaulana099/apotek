<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ProdukTemplate implements FromCollection, WithHeadings, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $request;

    function __construct($request)
    {
        $this->request = $request;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->getDelegate()->getStyle('A2:T2')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFA559');
            },
        ];
    }

    public function headings(): array
    {
        return [
            'NO',
            'NAMA',
            'QTY MG',
            'SATUAN',
            'USIA',
            'HARGA BELI',
            'HARGA JUAL',
            'KATEGORI',
            'KETERANGAN',
            ''
        ];

    }

    public function collection()
    {
        return collect([
            [
                'no' => '1',
                'nama' => 'Bufect Forte Sirup',
                'qty_mg' => 10,
                'satuan' => 'MG',
                'usia' => 'Anak',
                'harga_beli' => '10000',
                'harga_jual' => '12000',
                'kategori' => 'Obat Sirup (Demam)',
                'keterangan' => 'DAFTAR SIRUP OBAT BEBAS (ANALGETIK & ANTIPIRETIK)',
                '' => '<- Contoh data jangan dihapus - isi di bawah sample'
                ]
            ]);
        }
}
