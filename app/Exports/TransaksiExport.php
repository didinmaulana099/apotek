<?php

namespace App\Exports;

use App\Models\Transaksi;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Queue\SerializesModels;

class TransaksiExport extends DefaultValueBinder implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable, SerializesModels;

    public $request;

    function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $request = $this->request;
        //
        $index = Transaksi::with('pesanan')->where(function ($where) use ($request) {
            if (!empty($request->start_date) && !empty($request->end_date)) {
                $where->whereBetween('created_at', [
                    Carbon::parse(date($request->start_date). '00:00:00'),
                    Carbon::parse(date($request->end_date). '23:59:59')
                ]);
            }
        });
        
        $unmap = (clone $index)
        ->get();
        
        $transaksi = $unmap->map(function ($item, $key) {
            return $item->pesanan->map(function ($psn, $key) use ($item){
                $no = $key + 1;
                return [
                    'no' => $no,
                    'tgl_trx' => $item->created_at,
                    'no_trx' => $item->no_trx,
                    'grand_total' => $item->grand_total,
                    'kode_produk' => $psn->kode_produk,
                    'nama_produk' => $psn->nama_produk,
                    'satuan' => $psn->satuan,
                    'harga' => $psn->harga,
                    'qty' => $psn->qty,
                    'jumlah' => $psn->jumlah,
                    'created_by' => $psn->created_by,
                ];
            });
        })
        ->collapse();
        return $transaksi;
    }

    public function headings(): array
    {
        return [
            'NO',
            'TANGGAL',
            'NO TRX',
            'TOTAL',
            'KODE',
            'NAMA',
            'SATUAN',
            'HARGA',
            'QTY',
            'JUMLAH',
            'CREATED BY'
        ];
    }
}
