<?php

namespace App\Exports;

use App\Models\MasterKategori;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Queue\SerializesModels;

class KategoriExport extends DefaultValueBinder implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable, SerializesModels;

    public $request;

    function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $request = $this->request;
        //
        $index = MasterKategori::where(function ($where) use ($request) {

            if (!empty($request->keyword)) {
                foreach ($request->columns as $index => $column) {
                    if ($index == 0) {
                        $where->where($column, 'like', '%' . $request->keyword . '%');
                    } else {
                        $where->orWhere($column, 'like', '%' . $request->keyword . '%');
                    }
                }
            }
        })
        ->when(!empty($request->sort), function ($query) use ($request) {
            $query->orderBy($request->sort, $request->order == 'ascend' ? 'asc' : 'desc');
        });

        $unmap = (clone $index)
        // ->take(300)
        ->get();

        $masterProduk = $unmap->map(function ($item, $key) {;
            return [
                'no' => $key+1,
                'nama' => $item->nama,
                'usia' => $item->usia,
                'keterangan' => $item->keterangan,
                'created_by' => auth()->user()->name,
            ];
        });
        return $masterProduk;
    }

    public function headings(): array
    {
        return [
            'NO',
            'NAMA',
            'USIA',
            'KETERANGAN',
            'CREATED BY'
        ];
    }
}
