<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempPesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_pesanan', function (Blueprint $table) {
            $table->id();
            $table->string('kode_produk');
            $table->integer('produk_id');
            $table->integer('kategori_id');
            $table->string('nama_produk');
            $table->integer('satuan');
            $table->integer('harga');
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_pesanan');
    }
}
