<html>
<head>
    <title>Faktur Pembayaran</title>
</head>
<body onload="window.print();">
    <style>
        @media print {
            @page {
                margin: none;
                /* sheet-size: 48mm 50mm; imprtant to set paper size */
                size: A8;
            }
            html {
                direction: rtl;
            }
            html,body{margin:0;padding:0}
            #printContainer {
                width: 58mm;
                margin: auto;
                /*padding: 10px;*/
                /*border: 2px dotted #000;*/
                text-align: justify;
            }

           .text-center{text-align: center;}
        }
    </style>
    <div id="printContainer">
    {{-- <center> --}}
        <table style='width:140px; font-size:6pt; font-family:calibri; border-collapse: collapse; border:0;'>
            <td style="width:10%; text-align: center;">
                <span style='color:black;'>
                    <b>APOTEK XXXXXXX XXXXXXXXXXX</b>
                    <br/>
                    JL XXXXXXXXXXX XXXXXXX
                </span>
                <br/>
                @php
                use Carbon\Carbon;
                $date = Carbon::now()->format('d M Y');
                $time = Carbon::now()->format('H:i:s');
                @endphp
                <span style='font-size:5pt'>No. : {{$transaksi->no_trx}}, {{$date}} (user:xxxxx), {{$time}}</span>
                <br/>
            </td>
        </table>
        <style>
            hr {
                display: block;
                margin-top: 0.5em;
                margin-bottom: 0.5em;
                margin-left: auto;
                margin-right: auto;
                border-style: inset;
                border-width: 1px;
            }
        </style>
        <table cellspacing='0' cellpadding='0' style='width:10px; font-size:5pt; font-family:calibri;  border-collapse: collapse;' border='0'>
            <br/>
            <tr align='center'>
                <td width='4%'>Kode</td>
                <td width='14%'>Item</td>
                <td width='8%'>Price</td>
                <td width='1%'>Qty</td>
                <td width='10%'>Total</td>
                <tr>
                    <td colspan='5'>
                        <hr>
                    </td>
                </tr>
            </tr>
            @foreach ($transaksi->pesanan as $item)
            <tr>
                <td style='vertical-align:top; text-align:right; padding-right:10px'>{{$item->kode_produk}}</td>
                <td style='vertical-align:top'>{{$item->nama_produk}}</td>
                <td style='vertical-align:top; text-align:right; padding-right:10px'>{{number_format($item->harga,0,',','.')}}</td>
                <td style='vertical-align:top; text-align:right; padding-right:10px'>{{$item->qty}}</td>
                <td style='text-align:right; vertical-align:top'>{{number_format($item->jumlah,0,',','.')}}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan='5'><hr></td>
            </tr>
            <tr>
                {{-- <td colspan = '4'><div style='text-align:right'>Biaya Adm : </div></td><td style='text-align:right; font-size:6pt;'>Rp3.500,00</td> --}}
            </tr>
            <tr>
                <td colspan = '4'>
                    <div style='text-align:right; color:black'>Total : </div>
                </td>
                <td style='text-align:right; font-size:5pt; color:black'>{{number_format($transaksi->grand_total,0,',','.')}}</td>
            </tr>
            <tr>
                <td colspan = '4'><div style='text-align:right; color:black'>Cash : </div>
                </td>
                <td style='text-align:right; font-size:5pt; color:black'>{{number_format($transaksi->cash,0,',','.')}}</td>
            </tr>
            <tr>
                <td colspan = '4'>
                    <div style='text-align:right; color:black'>Change : </div>
                </td>
                <td style='text-align:right; font-size:5pt; color:black'>{{number_format($transaksi->change,0,',','.')}}</td>
            </tr>
            {{-- <tr>
                <td colspan = '4'><div style='text-align:right; color:black'>DP : </div></td><td style='text-align:right; font-size:6pt; color:black'>0</td>
            </tr>
            <tr>
                <td colspan = '4'><div style='text-align:right; color:black'>Sisa : </div></td><td style='text-align:right; font-size:6pt; color:black'>0</td>
            </tr> --}}
        </table>
        <table style='width:100; font-size:5pt;' cellspacing='2'>
            <br/>
            <br/>
            <tr>
                <td align='center'>
                    ****** TERIMAKASIH ******
                </td>
            </tr>
        </table>
    {{-- </center> --}}
    </div>
</body>
</html>
