<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Admin Dashboard</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/css/bootstrap.css')}}">

    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/iconly/bold.css')}}">

    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/bootstrap-icons/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/css/app.css')}}">
    <link rel="shortcut icon" href="{{ URL::asset('public/assets/admin/images/favicon.svg" type="image/x-icon')}}">
</head>

<body>
    <div id="app">
        <div style="background-color: #F4F5FA; padding: 2rem;">
            <section class="section">
                @if (count($temp_pesanan) > 0)
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                <h4>Invoice Pemesanan</h4>
                            </div>
                            <div class="buttons col-12 col-md-2">
                                <a class="btn btn-primary block " data-bs-toggle="modal" data-bs-target="#defaultScan">Tambah Produk</a>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('transaksi.pesanan')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <table class="table table-striped calculation" id="table1">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">No</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Satuan</th>
                                        <th>Harga</th>
                                        <th>QTY</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($temp_pesanan as $key => $item)
                                    <tr class="jumlahHarga">
                                        <td style="text-align: center;">{{$key+1}}</td>
                                        <td>
                                            <input type="hidden" name="produk_id[]" value="{{$item->id}}" class="form-control" style="margin-right: 35px;">
                                            <input type="hidden" name="kategori_id[]" value="{{$item->kategori_id}}" class="form-control" style="margin-right: 35px;">
                                            <input type="hidden" name="kode_produk[]" value="{{$item->kode_produk}}" class="form-control" style="margin-right: 35px;">
                                            {{$item->kode_produk}}
                                        </td>
                                        <td>
                                            <input type="hidden" name="nama_produk[]" value="{{$item->nama_produk}}" class="form-control" style="margin-right: 35px;">
                                            {{$item->nama_produk}}
                                        </td>
                                        <td>
                                            <input type="hidden" name="satuan[]" value="{{$item->satuan}}" class="form-control" style="margin-right: 35px;">
                                            {{$item->satuan}}
                                        </td>
                                        <td>
                                            <input type="hidden" name="harga[]" value="{{$item->harga}}" class="form-control targetHarga" style="margin-right: 35px;">
                                            {{ "Rp " . number_format($item->harga) }}</td>
                                            <td>
                                                <input type="number" name="qty[]" class="form-control targetQty" style="margin-right: 35px; background-color: #edffec;" value="{{$item->qty}}" required="">
                                            </td>
                                            <td>
                                                <input type="text" name="jumlah[]" class="form-control targetJumlah" style="margin-right: 35px;" value="{{$item->jumlah}}" readonly="" required="">
                                            </td>
                                            <td>
                                                <a href="{{route('transaksi.hapus.pesanan', $item->id)}}" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="col-lg-4 col-md-4 col-12 totalCash" style="float: right; width: 33%;">
                                    <ul class="">
                                        <li class="list-group-item d-flex border-0">
                                            <span style="padding: 10px;">Total</span>
                                            <input style="font-size: 20px; font-weight: bold;" class="form-control" type="number" id="targetSubtotal" name="subtotal" value="" readonly="" required="">
                                        </li>
                                        <li class="list-group-item d-flex border-0">
                                            <span style="padding: 10px;">Cash</span>
                                            <input type="number" name="cash" class="form-control" id="cash" value="" required="">
                                        </li>
                                        <li class="list-group-item py-0 border-0 mt-25">
                                            <hr/>
                                        </li>
                                        <li class="list-group-item d-flex border-0">
                                            <span style="padding: 10px;">Sisa</span>
                                            <input class="form-control" type="number" id="change" name="change" readonly="" value="" required="">
                                        </li>
                                    </ul>
                                </div>
                                <button class="btn btn-primary" type="submit">SUBMIT</button>
                            </div>
                        </form>
                        @else
                        <div class="card" style="display: flex; align-items: center; justify-content: center;">
                            <style>
                                .clock {
                                    /* position: absolute; */
                                    top: 50%;
                                    left: 50%;
                                    /* transform: translateX(-50%) translateY(-50%); */
                                    color: #4C3D3D;
                                    font-size: 150px;
                                    letter-spacing: 6px;
                                }
                            </style>
                            <div id="DisplayClock" class="clock" onload="showTime()"></div>
                            <span>scan now untuk keluarkan struk</span>
                            <div class="card-body">
                                <a class="btn btn-outline-primary block " data-bs-toggle="modal" data-bs-target="#defaultScan">SCAN NOW</a>
                                <a href="{{route('transaksi.list')}}" class="btn btn-outline-success block">List Transaksi</a>
                            </div>
                        </div>
                        @endif
                    </section>
                </div>
            </div>

            <div class="modal fade text-left" id="defaultScan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="modal-header">
                                <h5 class="modal-title">SCAN PESANAN</h5>
                                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                                    <i class="bi bi-door-closed-fill" data-feather="x">CLOSE</i>
                                </button>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table2">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                            <th>Kode</th>
                                            <th>Nama</th>
                                            <th>kategori</th>
                                            <th>Usia</th>
                                            <th>QTY</th>
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($produk as $key => $item)
                                        <tr>
                                            <td style="text-align: center;">{{$key+1}}</td>
                                            <td>{{$item->kode}}</td>
                                            <td>{{$item->nama}}</td>
                                            <td>{{$item->kategori['nama']}}</td>
                                            <td>{{$item->usia}}</td>
                                            <td>{{$item->qty_mg}}{{$item->satuan}}</td>
                                            <td>{{ "Rp " . number_format($item->harga_jual) }}</td>
                                            @if ($item->stock['stock_sisa'] > 0)
                                            <td style="text-align: center;">{{$item->stock['stock_sisa']}}</td>
                                            <td>
                                                <a href="{{route('transaksi.temp.pesanan', $item->id)}}" class="btn btn-success btn-sm"><i class="bi bi-check"></i></a>
                                            </td>
                                            @else
                                            <td style="color: white; background-color: red; text-align: center;">0</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <script>
                function showTime(){
                    var date = new Date();
                    var h = date.getHours();
                    var m = date.getMinutes();
                    var s = date.getSeconds();
                    var session = "AM";

                    if(h == 0){
                        h = 12;
                    }
                    if (h > 12) {
                        h = h - 12 ;
                        session = "PM";
                    }

                    h = (h<10) ? "0" + h : h;
                    m = (m<10) ? "0" + m : m;
                    s = (s<10) ? "0" + s : s;

                    var time = h + ":" + m + ":" + s + " " + session;

                    document.getElementById("DisplayClock").innerText = time;
                    document.getElementById("DisplayClock").textContent = time;

                    setTimeout(showTime, 1000);
                }

                showTime();
            </script>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

            <script type="text/javascript">
                $(document).ready(function() {
                    // let wrap = $(this).closest('tr')
                    // let harga = $(wrap.find('.targetHarga')).val();
                    // let qty = $(wrap.find('.targetQty')).val();
                    // const jumlah = harga * qty;
                    // let jumlahSub = parseInt($(this).find('.targetJumlah').val(jumlah))

                    $('.calculation').on('keyup', '.jumlahHarga', function(){
                        let wrap = $(this).closest('tr')
                        let harga = $(wrap.find('.targetHarga')).val();
                        let qty = $(wrap.find('.targetQty')).val();
                        const jumlah = harga * qty;
                        let jumlahSub = parseInt($(this).find('.targetJumlah').val(jumlah))
                    })
                });

                $(document).ready(function() {
                    $('.calculation').on('keyup', '.jumlahHarga', function(){
                        var subTot  = 0;
                        $(".targetJumlah").each(function() {
                            subTot += +$(this).val();
                        });
                        $("#targetSubtotal").val(subTot);
                    })
                });

                // kembali
                $(document).ready(function() {
                    $(".totalCash").keyup(function(){
                        var subTotal  = $("#targetSubtotal").val();
                        var cash  = $(this).find('#cash').val();
                        var sisa = subTotal - cash;
                        var change = Math.abs(sisa);
                        // total stelah diskon
                        $("#change").val(change);
                    });
                });

            </script>

            <script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
            <script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

            {{-- <script src="{{ URL::asset('public/assets/admin/vendors/apexcharts/apexcharts.js')}}"></script> --}}
            <script src="{{ URL::asset('public/assets/admin/js/pages/dashboard.js')}}"></script>

            {{-- <script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script> --}}

            <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
            <script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
            <script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

            <script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
            <script>
                let table2 = document.querySelector('#table2');
                let dataTable = new simpleDatatables.DataTable(table2);
            </script>

        </body>
