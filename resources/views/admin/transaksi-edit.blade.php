@extends('admin.layout')
<title>Transaksi</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-10">
                    <h4>Update untuk no {{$trx->no_trx}}</h4>
                </div>
                <div class="buttons col-12 col-md-2">
                    <a class="btn btn-primary block " data-bs-toggle="modal" data-bs-target="#defaultScan">Tambah</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('transaksi.update', $trx->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <table class="table table-striped calculation" id="table1">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Satuan</th>
                            <th>Harga</th>
                            <th>QTY</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($trx->pesanan as $key => $item)
                        <tr class="jumlahHarga">
                            <td style="text-align: center;">{{$key+1}}</td>
                            <td>
                                <input type="hidden" name="produk_id[]" value="{{$item->id}}" class="form-control" style="margin-right: 35px;">
                                <input type="hidden" name="kategori_id[]" value="{{$item->kategori_id}}" class="form-control" style="margin-right: 35px;">
                                <input type="hidden" name="kode_produk[]" value="{{$item->kode_produk}}" class="form-control" style="margin-right: 35px;">
                                {{$item->kode_produk}}
                            </td>
                            <td>
                                <input type="hidden" name="nama_produk[]" value="{{$item->nama_produk}}" class="form-control" style="margin-right: 35px;">
                                {{$item->nama_produk}}
                            </td>
                            <td>
                                <input type="hidden" name="satuan[]" value="{{$item->satuan}}" class="form-control" style="margin-right: 35px;">
                                {{$item->satuan}}
                            </td>
                            <td>
                                <input type="hidden" name="harga[]" value="{{$item->harga}}" class="form-control targetHarga" style="margin-right: 35px;">
                                {{number_format($item->harga) }}
                            </td>
                            <td style="width: 11%;">
                                <input type="number" name="qty[]" class="form-control targetQty" style="margin-right: 35px; background-color: #edffec;" value="{{$item->qty}}" required="">
                            </td>
                            <td style="width: 22%;">
                                <input type="text" name="jumlah[]" class="form-control targetJumlah" style="margin-right: 35px;" value="{{$item->jumlah}}" readonly="" required="">
                            </td>
                            <td>
                                <a href="{{route('transaksi.pesanan.destroy', $item->id)}}" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="col-lg-4 col-md-4 col-12 totalCash" style="float: right; width: 33%;">
                    <ul class="">
                        <li class="list-group-item d-flex border-0">
                            <span style="padding: 10px;">Total</span>
                            <input style="font-size: 20px; font-weight: bold;" class="form-control" type="number" id="targetSubtotal" name="subtotal" value="" readonly="" required>
                        </li>
                        <li class="list-group-item d-flex border-0">
                            <span style="padding: 10px;">Cash</span>
                            <input type="number" name="cash" class="form-control" id="cash" value="" required>
                        </li>
                        <li class="list-group-item py-0 border-0 mt-25">
                            <hr/>
                        </li>
                        <li class="list-group-item d-flex border-0">
                            <span style="padding: 10px;">Sisa</span>
                            <input class="form-control" type="number" id="change" name="change" readonly="" value="" required>
                        </li>
                    </ul>
                    <hr/>
                    <div style="float: right">
                        <button type="button" class="btn" onclick="history.back()">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Close</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Accept</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</section>

<div class="modal fade text-left" id="defaultScan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h5 class="modal-title">SCAN PESANAN</h5>
                    <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                        <i class="bi bi-door-closed-fill" data-feather="x">CLOSE</i>
                    </button>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table2">
                        <thead>
                            <tr>
                                <th style="text-align: center;">No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>kategori</th>
                                <th>Usia</th>
                                <th>QTY</th>
                                <th>Harga</th>
                                <th>Stok</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($produk as $key => $item)
                            <tr>
                                <td style="text-align: center;">{{$key+1}}</td>
                                <td>{{$item->kode}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{$item->kategori['nama']}}</td>
                                <td>{{$item->usia}}</td>
                                <td>{{$item->qty_mg}}{{$item->satuan}}</td>
                                <td>{{ "Rp " . number_format($item->harga_jual) }}</td>
                                @if ($item->stock['stock_sisa'] > 0)
                                <td style="text-align: center;">{{$item->stock['stock_sisa']}}</td>
                                <td>
                                    <form action="{{route('transaksi.pesanan.edit', $item->id)}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{$trx->id}}" name="id_trx">
                                        <button type="submit" class="btn btn-success ml-1">
                                            <i class="bi bi-check"></i>
                                        </button>
                                    </form>
                                </td>
                                @else
                                <td style="color: white; background-color: red; text-align: center;">0</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // let wrap = $(this).closest('tr')
        // let harga = $(wrap.find('.targetHarga')).val();
        // let qty = $(wrap.find('.targetQty')).val();
        // const jumlah = harga * qty;
        // let jumlahSub = parseInt($(this).find('.targetJumlah').val(jumlah))

        $('.calculation').on('keyup', '.jumlahHarga', function(){
            let wrap = $(this).closest('tr')
            let harga = $(wrap.find('.targetHarga')).val();
            let qty = $(wrap.find('.targetQty')).val();
            const jumlah = harga * qty;
            let jumlahSub = parseInt($(this).find('.targetJumlah').val(jumlah))
        })
    });

    $(document).ready(function() {
        $('.calculation').on('keyup', '.jumlahHarga', function(){
            var subTot  = 0;
            $(".targetJumlah").each(function() {
                subTot += +$(this).val();
            });
            $("#targetSubtotal").val(subTot);
        })
    });

    // kembali
    $(document).ready(function() {
        $(".totalCash").keyup(function(){
            var subTotal  = $("#targetSubtotal").val();
            var cash  = $(this).find('#cash').val();
            var sisa = subTotal - cash;
            var change = Math.abs(sisa);
            // total stelah diskon
            $("#change").val(change);
        });
    });

</script>

<script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

{{-- <script src="{{ URL::asset('public/assets/admin/vendors/apexcharts/apexcharts.js')}}"></script> --}}
<script src="{{ URL::asset('public/assets/admin/js/pages/dashboard.js')}}"></script>

{{-- <script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script> --}}

<link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
<script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
<script>
    let table2 = document.querySelector('#table2');
    let dataTable = new simpleDatatables.DataTable(table2);
</script>



@endsection
