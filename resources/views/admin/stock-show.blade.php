@extends('admin.layout')
<title>Stock</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-9">
                    Update Stock
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('stock.update', $stock->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama">Nama Produk</label>
                            <input type="text" name="nama_produk" class="form-control" id="nama_produk" value="{{$stock->nama_produk}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stockAwal">Stock Awal</label>
                            <input type="number" name="stock_awal" class="form-control" id="stock_awal" value="{{$stock->stock_awal}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stockMasuk">Stock Masuk</label>
                            <input type="number" name="stock_masuk" class="form-control" id="stock_masuk" value="{{$stock->stock_masuk}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stockKeluar">Stock Keluar</label>
                            <input type="number" name="stock_keluar" class="form-control" id="stock_keluar" value="{{$stock->stock_keluar}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stockSisa">Sisa Stock</label>
                            <input type="number" name="stock_sisa" class="form-control" id="stock_sisa" value="{{$stock->stock_sisa}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Close</span>
                    </button>
                    <button type="submit" class="btn btn-primary ml-1">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Accept</span>
                    </button>
                </div>
            </form>
        </div>
    </div>

</section>



@endsection
