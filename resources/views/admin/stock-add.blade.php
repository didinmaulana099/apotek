@extends('admin.layout')
<title>Stock Tambah</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-10">
                    <h4>Stock Tambah</h4>
                </div>
                <div class="buttons col-12 col-md-2">
                    <a class="btn btn-outline-primary block " data-bs-toggle="modal" data-bs-target="#defaultProduk">Tambah</a>
                </div>
            </div>
        </div>
        <form action="{{route('stock.store')}}" method="post">
            @csrf
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Stock</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($temp_stock as $key => $item)
                        <tr>
                            <td style="text-align: center;">{{$key+1}}</td>
                            <td>
                                <input type="hidden" name="kode_produk[]" value="{{$item->kode_produk}}" class="form-control" style="margin-right: 35px;">
                                {{$item->kode_produk}}
                            </td>
                            <td>
                                <input type="hidden" name="nama_produk[]" value="{{$item->nama_produk}}" class="form-control" style="margin-right: 35px;">
                                {{$item->nama_produk}}
                            </td>
                            <td>
                                <input type="number" name="stock[]" class="form-control" style="margin-right: 35px;" required="">
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="">
                                            <a href="{{ route('stock.temp.hapus', $item->id)}}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this item')"><i class="bi bi-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @if (count($temp_stock) > 0)
                <button class="btn btn-primary" type="submit">SUBMIT</button>
                @else
                <p>click <b>tambah</b> untuk menambahkan stock</p>
                @endif
            </div>
        </form>

    </div>
</section>

<div class="modal fade text-left" id="defaultProduk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h5 class="modal-title">SCAN PESANAN</h5>
                    <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                        <i class="bi bi-door-closed-fill" data-feather="x">CLOSE</i>
                    </button>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table2">
                        <thead>
                            <tr>
                                <th style="text-align: center;">No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>kategori</th>
                                <th>Usia</th>
                                <th>QTY</th>
                                <th>Harga</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($produk as $key => $item)
                            <tr>
                                <td style="text-align: center;">{{$key+1}}</td>
                                <td>{{$item->kode}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{$item->kategori['nama']}}</td>
                                <td>{{$item->usia}}</td>
                                <td>{{$item->qty_mg}}{{$item->satuan}}</td>
                                <td>{{ "Rp " . number_format($item->harga_jual) }}</td>
                                <td>
                                    <a href="{{route('stock.temp.store', $item->id)}}" class="btn btn-success btn-sm"><i class="bi bi-check"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
<script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
<script>
    // Simple Datatable
    let table2 = document.querySelector('#table2');
    let dataTable = new simpleDatatables.DataTable(table2);

    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>

<script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script>

@endsection
