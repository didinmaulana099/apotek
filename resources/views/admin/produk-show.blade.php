@extends('admin.layout')
<title>Master Produk</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-9">
                    Update Master Data Obat
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('produk.update', $produk->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="text" name="kode" class="form-control" id="kode" value="{{$produk->kode}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" id="nama" value="{{$produk->nama}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Kategori">Kategori</label>
                            <fieldset class="form-group">
                                <select class="form-select" id="kategori" name="kategori_id">
                                    <option value="{{$produk->kategori_id}}">{{$produk->kategori['nama']}}</option>
                                    @foreach ($kategori as $item)
                                    <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="form-group">
                            <label for="Usia">Usia</label>
                            <fieldset class="form-group">
                                <select class="form-select" id="usia" name="usia">
                                    <option value="{{$produk->usia}}">{{$produk->usia}}</option>
                                    @foreach ($usia as $item)
                                    <option value="{{$item->nama}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="qty_mg">QTY MG</label>
                            <input type="number" name="qty_mg" id="qtyMg" class="form-control" value="{{$produk->qty_mg}}">
                        </div>
                        <div class="form-group">
                            <label for="satuan">Satuan MG/ML</label>
                            <fieldset class="form-group">
                                <select class="form-select" id="satuan" name="satuan">
                                    <option value="{{$produk->satuan}}">{{$produk->satuan}}</option>
                                    @foreach ($satuan as $item)
                                    <option value="{{$item->nama}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hargaBeli">Harga Beli</label>
                            <input type="number" name="harga_beli" id="hargaBeli" class="form-control" value="{{$produk->harga_beli}}">
                            <p><small class="text-muted">Input harga beli dengan number</small>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hargaJual">Harga Jual</label>
                            <input type="number" name="harga_jual" id="hargaJual" class="form-control" value="{{$produk->harga_jual}}">
                            <p><small class="text-muted">Input harga Jual dengan number</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Close</span>
                    </button>
                    <button type="submit" class="btn btn-primary ml-1">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Accept</span>
                    </button>
                </div>
            </form>
        </div>
    </div>

</section>



@endsection
