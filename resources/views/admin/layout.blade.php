<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Admin Dashboard</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/css/bootstrap.css')}}">

    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/iconly/bold.css')}}">

    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/bootstrap-icons/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/css/app.css')}}">
    <link rel="shortcut icon" href="{{ URL::asset('public/assets/admin/images/favicon.svg" type="image/x-icon')}}">
</head>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="/">
                                <h2>APOTEK</h2>
                                {{-- <img src="{{ URL::asset('public/assets/admin/images/logo/logo.png')}}" alt="Logo" srcset=""> --}}
                            </a>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>

                        <li class="sidebar-item active ">
                            <a href="/" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        @if (auth()->user()->role === 'superadmin')
                        <li class="sidebar-title">Master Data</li>

                        <li class="sidebar-item">
                            <a href="/master-produk" class='sidebar-link'>
                                <i class="bi bi-hexagon-fill"></i>
                                <span>Master Produk</span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a href="/master-kategori" class='sidebar-link'>
                                <i class="bi bi-pen-fill"></i>
                                <span>Master Kategori</span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a href="{{route('usia.index')}}" class='sidebar-link'>
                                <i class="bi bi-grid-1x2-fill"></i>
                                <span>Master Usia</span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a href="{{route('satuan.index')}}" class='sidebar-link'>
                                <i class="bi bi-file-earmark-spreadsheet-fill"></i>
                                <span>Master Satuan</span>
                            </a>
                        </li>


                        <li class="sidebar-title">Transaksi</li>

                        <li class="sidebar-item">
                            <a href="{{route('stock.index')}}" class='sidebar-link'>
                                <i class="bi bi-pentagon-fill"></i>
                                <span>Stock</span>
                            </a>
                        </li>

                        @else
                        @endif
                        <li class="sidebar-item  ">
                            <a href="{{ route('transaksi.scan')}}" class='sidebar-link'>
                                <i class="bi bi-printer-fill"></i>
                                <span>Cetak</span>
                            </a>
                        </li>

                        <li class="sidebar-item  ">
                            <a href="{{ route('transaksi.list')}}" class='sidebar-link'>
                                <i class="bi bi-file-text"></i>
                                <span>Transaksi</span>
                            </a>
                        </li>

                        {{-- <li class="sidebar-item  ">
                            <a href="ui-file-uploader.html" class='sidebar-link'>
                                <i class="bi bi-cloud-arrow-up-fill"></i>
                                <span>Reorder</span>
                            </a>
                        </li> --}}
                        <li class="sidebar-item  ">
                            <a href="{{route('transaksi.report')}}" class='sidebar-link'>
                                <i class="bi bi-cloud-arrow-up-fill"></i>
                                <span>Report</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main" style="background-color: #F4F5FA;">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
                <a href="{{route('logout')}}">
                    <img src="{{ URL::asset('public/assets/admin/images/logo/logout.png')}}" style="width: 3%;">
                    signout
                </a>
            </header>
            @yield('content')

            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-start">
                        <p>2023 &copy; Apotek Lentera Farma</p>
                    </div>
                    <div class="float-end">
                        <p>Crafted by Ettos</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
        <script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

        <script src="{{ URL::asset('public/assets/admin/vendors/apexcharts/apexcharts.js')}}"></script>
        <script src="{{ URL::asset('public/assets/admin/js/pages/dashboard.js')}}"></script>

        <script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script>
        @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    </body>

    </html>
