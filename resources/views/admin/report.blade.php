@extends('admin.layout')
<title>Transaksi Report</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-9">
                    <h4>Report Transaksi</h4>
                </div>
                <div class="buttons col-12 col-md-3">
                    <a class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#defaultFilter">Filter Date</a>
                    <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#defaultExport">Export</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th>Tgl Transaksi</th>
                        <th>No Inv</th>
                        <th>kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Jumlah</th>
                        <th>Pembuat</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    use Carbon\Carbon;
                    @endphp
                    @foreach ($report as $key => $item)
                    <tr>
                        <td style="text-align: center;">{{$key+1}}</td>
                        <td>{{ Carbon::parse($item->tgl_trx)->format('d/M/y')}}</td>
                        <td>{{$item->no_trx}}</td>
                        <td>{{$item->kode_produk}}</td>
                        <td>{{$item->nama_produk}}</td>
                        <td>{{$item->satuan}}</td>
                        <td>{{number_format($item->harga)}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{number_format($item->jumlah)}}</td>
                        <td>{{$item->created_by}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>

<div class="modal fade text-left" id="defaultFilter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('transaksi.report')}}" method="get" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Bulan">Start Date</label>
                                <input class="form-control" type="date" name="start_date">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Tahun">End Date</label>
                                <input class="form-control" type="date" name="end_date">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Close</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Accept</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="defaultExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('transaksi.export')}}" method="get" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Bulan">Start Date</label>
                                <input class="form-control" type="date" name="start_date">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Tahun">End Date</label>
                                <input class="form-control" type="date" name="end_date">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Close</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Accept</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
<script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>

<script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script>

@endsection
