@extends('admin.layout')
<title>Master Usia</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-10">
                    <h4>Master Data Usia</h4>
                </div>
                <div class="buttons col-12 col-md-2">
                    <a class="btn btn-outline-primary block " data-bs-toggle="modal" data-bs-target="#defaultCreate">Tambah</a>
                </div>
            </div>
        </div>
        {{-- <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
            Tambah
        </button> --}}
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($usia as $key => $item)
                    <tr>
                        <td style="text-align: center;">{{$key+1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="">
                                        <a href="{{ route('usia.show', $item->id)}}" class="btn btn-outline-primary btn-sm block"><i class="bi bi-pencil"></i></a>
                                        <a href="{{ route('usia.destroy', $item->id)}}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this item')"><i class="bi bi-trash"></i></a>
                                    </div>
                                    {{-- <div class="col-md-2">
                                        <form action="{{route('usia.destroy')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" id="p_id" value="{{$item->id}}">
                                            <button class="btn btn-danger btn-sm" name="submit" type="submit" onclick="archiveFunction()">
                                                <i class="bi bi-trash"></i>
                                            </button>
                                        </form>
                                    </div> --}}
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    <div class="modal fade text-left" id="defaultCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel1">Tambah Data Master usia Obat</h5>
                    <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('usia.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="nama usia obat">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn" data-bs-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Close</span>
                                </button>
                                <button type="submit" class="btn btn-primary ml-1">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Accept</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function archiveFunction() {
                event.preventDefault(); // prevent form submit
                var form = event.target.form; // storing the form
                swal({
                    title: "Are you sure?",
                    text: "But you will still be able to retrieve this file.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, archive it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {
                        form.submit();          // submitting the form when user press yes
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });
            }
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

        <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
        <script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
        <script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

        <script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
        <script>
            // Simple Datatable
            let table1 = document.querySelector('#table1');
            let dataTable = new simpleDatatables.DataTable(table1);
        </script>

        <script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script>

        @endsection
