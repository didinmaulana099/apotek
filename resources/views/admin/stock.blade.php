@extends('admin.layout')
<title>Stock Opname</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-10">
                    <h4>Stock Opname</h4>
                </div>
                <div class="buttons col-12 col-md-2">
                    <a href="{{route('stock.temp')}}" class="btn btn-outline-primary block">Tambah</a>
                </div>
            </div>
        </div>
        {{-- <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
            Tambah
        </button> --}}
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Stock Awal</th>
                        <th>Stock Masuk</th>
                        <th>Stock Keluar</th>
                        <th>Sisa Stock</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stock as $key => $item)
                    <tr>
                        <td style="text-align: center;">{{$key+1}}</td>
                        <td>{{$item->kode_produk}}</td>
                        <td>{{$item->nama_produk}}</td>
                        <td>{{$item->stock_awal}}</td>
                        <td>{{$item->stock_masuk}}</td>
                        <td>{{$item->stock_keluar}}</td>
                        <td>{{$item->stock_sisa}}</td>
                        <td>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="">
                                        <a href="{{ route('stock.show', $item->id)}}" class="btn btn-outline-primary btn-sm block"><i class="bi bi-pencil"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
    <script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
    <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

    <script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script>

    @endsection
