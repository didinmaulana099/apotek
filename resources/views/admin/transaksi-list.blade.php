@extends('admin.layout')
<title>Transaksi List</title>
@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-9">
                    <h4>List Transaksi</h4>
                </div>
                <div class="buttons col-12 col-md-3">
                    <a href="{{route('transaksi.scan')}}" class="btn btn-outline-primary block">
                        Tambah
                    </a>
                    <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#defaultCreate">Export</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th>No Inv</th>
                        <th>Jumlah Item</th>
                        <th>Grand Total</th>
                        <th>Cash</th>
                        <th>Change</th>
                        <th>Tgl Transaksi</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    use Carbon\Carbon;
                    @endphp
                    @foreach ($trx as $key => $item)
                    <tr>
                        <td style="text-align: center;">{{$key+1}}</td>
                        <td>{{$item->no_trx}}</td>
                        <td>{{$item->jumlah_item}}</td>
                        <td>{{"Rp. ". number_format($item->grand_total)}}</td>
                        <td>{{"Rp. ". number_format($item->cash)}}</td>
                        <td>{{"Rp. ". number_format($item->change)}}</td>
                        <td>{{ Carbon::parse($item->created_at)->format('d/M/y')}}</td>
                        <td>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="">
                                        <a href="{{route('transaksi.cetak', $item->id)}}" class="btn btn-warning btn-sm"><i class="bi bi-printer-fill"></i></a>
                                        <a href="{{route('transaksi.details', $item->id)}}" class="btn btn-secondary btn-sm"><i class="bi bi-info-circle-fill"></i></a>
                                        <a href="{{route('transaksi.show', $item->id)}}" class="btn btn-primary btn-sm"><i class="bi bi-pencil"></i></a>
                                        <a href="{{route('transaksi.hapus', $item->id)}}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this item')"><i class="bi bi-trash"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    <div class="modal fade text-left" id="defaultCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel1">Tambah Data Master Obat</h5>
                    <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('transaksi.export')}}" method="get" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Bulan">Start Date</label>
                                    <input class="form-control" type="date" name="start_date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Tahun">End Date</label>
                                    <input class="form-control" type="date" name="end_date">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn" data-bs-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Close</span>
                            </button>
                            <button type="submit" class="btn btn-primary ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Accept</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

    <link rel="stylesheet" href="{{ URL::asset('public/assets/admin/vendors/simple-datatables/style.css')}}">
    <script src="{{ URL::asset('public/assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/admin/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{ URL::asset('public/assets/admin/vendors/simple-datatables/simple-datatables.js')}}"></script>
    <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

    <script src="{{ URL::asset('public/assets/admin/js/main.js')}}"></script>

    @endsection
