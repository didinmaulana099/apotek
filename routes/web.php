<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', 'AuthController@login')->name('login');
Route::post('/auth-login', 'AuthController@authenticate')->name('authenticate');
Route::get('/auth-logout', 'AuthController@logout')->name('logout');
// Route::get('/auth-register', 'AuthController@auth')->name('auth-register');

//admin route
Route::group(['middleware' => ['auth', 'checkRole:pegawai,superadmin']], function(){
    //admin
    Route::get('/', 'Admin\DashboardController@dashboard')->name('dashboard');
    Route::get('/settings', 'Admin\SettingsController@index')->name('index');
    Route::get('/settings/header', 'Admin\SettingsController@header')->name('settings.header');
    // Route::get('/admin/berita', 'Admin\SettingsController@index')->name('index');

    //master data
    Route::get('/master-produk', 'Admin\MasterDataController@index');
    Route::post('/master-produk-store', 'Admin\MasterDataController@store')->name('produk.store');
    Route::get('/master-produk-template', 'Admin\MasterDataController@template')->name('produk.template');
    Route::post('/master-produk-import', 'Admin\MasterDataController@import')->name('produk.import');
    Route::get('/master-produk-export', 'Admin\MasterDataController@export')->name('produk.export');
    Route::get('/master-produk-show/{id}', 'Admin\MasterDataController@show')->name('produk.show');
    Route::post('/master-produk-update/{id}', 'Admin\MasterDataController@update')->name('produk.update');
    Route::get('/master-produk-hapus/{id}', 'Admin\MasterDataController@destroy')->name('produk.destroy');

    //kategori
    Route::get('/master-kategori', 'Admin\MasterKategoriController@index');
    Route::post('/master-kategori-store', 'Admin\MasterKategoriController@store')->name('kategori.store');
    Route::get('/master-kategori-export', 'Admin\MasterKategoriController@export')->name('kategori.export');
    Route::get('/master-kategori-show/{id}', 'Admin\MasterKategoriController@show')->name('kategori.show');
    Route::post('/master-kategori-update/{id}', 'Admin\MasterKategoriController@update')->name('kategori.update');
    Route::get('/master-kategori-hapus/{id}', 'Admin\MasterKategoriController@destroy')->name('kategori.destroy');

    //Usia
    Route::get('/master-usia', 'Admin\MasterUsiaController@index')->name('usia.index');
    Route::post('/master-usia-store', 'Admin\MasterUsiaController@store')->name('usia.store');
    Route::get('/master-usia-show/{id}', 'Admin\MasterUsiaController@show')->name('usia.show');
    Route::post('/master-usia-update/{id}', 'Admin\MasterUsiaController@update')->name('usia.update');
    Route::get('/master-usia-hapus/{id}', 'Admin\MasterUsiaController@destroy')->name('usia.destroy');

    //satuan
    Route::get('/master-satuan', 'Admin\MasterSatuanController@index')->name('satuan.index');
    Route::post('/master-satuan-store', 'Admin\MasterSatuanController@store')->name('satuan.store');
    Route::get('/master-satuan-show/{id}', 'Admin\MasterSatuanController@show')->name('satuan.show');
    Route::post('/master-satuan-update/{id}', 'Admin\MasterSatuanController@update')->name('satuan.update');
    Route::get('/master-satuan-hapus/{id}', 'Admin\MasterSatuanController@destroy')->name('satuan.destroy');

    // stock opname
    Route::get('/stock', 'Admin\StockController@index')->name('stock.index');
    Route::get('/stock/temp', 'Admin\StockController@tempStock')->name('stock.temp');
    Route::get('/stock/temp/store/{id}', 'Admin\StockController@tempStore')->name('stock.temp.store');
    Route::get('/stock/temp/hapus/{id}', 'Admin\StockController@tempHapus')->name('stock.temp.hapus');
    Route::post('/stock/store', 'Admin\StockController@store')->name('stock.store');
    Route::get('/stock/show/{id}', 'Admin\StockController@show')->name('stock.show');
    Route::post('/stock/update/{id}', 'Admin\StockController@update')->name('stock.update');
    Route::get('/stock/hapus/{id}', 'Admin\StockController@destroy')->name('stock.destroy');

    // transaksi
    Route::get('/scan', 'Admin\TransaksiController@scan')->name('transaksi.scan');
    Route::get('/transaksi-list', 'Admin\TransaksiController@list')->name('transaksi.list');
    Route::get('/transaksi-report', 'Admin\TransaksiController@report')->name('transaksi.report');
    Route::get('/transaksi-export', 'Admin\TransaksiController@export')->name('transaksi.export');
    Route::get('/transaksi-details/{id}', 'Admin\TransaksiController@details')->name('transaksi.details');
    Route::get('/transaksi-hapus/{id}', 'Admin\TransaksiController@hapusTrx')->name('transaksi.hapus');
    Route::get('/transaksi-show/{id}', 'Admin\TransaksiController@show')->name('transaksi.show');
    Route::post('/transaksi-update/{id}', 'Admin\TransaksiController@update')->name('transaksi.update');
    Route::get('/transaksi-hapus/pesanan/{id}', 'Admin\TransaksiController@editHapusPesananTrx')->name('transaksi.pesanan.destroy');
    Route::post('/transaksi-edit/pesanan/{id}', 'Admin\TransaksiController@editAddPesanan')->name('transaksi.pesanan.edit');

    Route::get('/temp-pesanan/{id}', 'Admin\TransaksiController@tempPesanan')->name('transaksi.temp.pesanan');
    Route::post('/pesanan/out', 'Admin\TransaksiController@trxPesanan')->name('transaksi.pesanan');
    Route::get('/hapus-pesanan/{id}', 'Admin\TransaksiController@hapusPesanan')->name('transaksi.hapus.pesanan');

    Route::get('/cetak-transaksi/{id}', 'Admin\TransaksiController@trxCetak')->name('transaksi.cetak');
});
